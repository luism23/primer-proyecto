import request from '@api/request'

const Index = (props) => {
    console.log(props);
    return (
        <div>
            <strong>
                {props.post.title}
            </strong>
            <br />
            <p>
                {props.post.body}
            </p>
            <br />
            <h1>Comentatios</h1>
            <br />

           

        </div>
    )
}
export async function getServerSideProps(context) {
    const id = context.params.id
    const result = await request({
        method: "get",
        url: `https://jsonplaceholder.typicode.com/posts?id=${id}`
    })
    if (result.status == 200) {
        const result = await request({
            method: "get",
            url: `https://jsonplaceholder.typicode.com/comments?postId=${id}`
        })
        return {
            props: {
                post: result.data[0]
            }
        }
    }
    return {
        redirect: {
            permanent: false,
            destination: "/login",
        },
        props: {},
    };

}

export default Index