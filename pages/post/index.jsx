import request from '@api/request'
import Link from 'next/link'

const post = ({ post}) => {
    return (
        <div>
            {post.map((e, i) => {
                return (
                    <div key={i}>
                        <Link href={`/post/${e.id}`}>
                            <a >
                                <strong>
                                    {e.title}
                                </strong>

                            </a>
                        </Link>
                        <br />
                        <br />
                    </div>
                )
            })}
        </div>
    )

}
export async function getServerSideProps(context) {
    const result = await request({
        method: "get",
        url: "https://jsonplaceholder.typicode.com/posts"
    })
    if (result.status == 200) {
        return {
            props: {
                post: result.data
            }
        }
    }
    return {
        redirect: {
            permanent: false,
            destination: "/login",
        },
        props: {},
    };

}

export default post