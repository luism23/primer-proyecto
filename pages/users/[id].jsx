import request from '@api/request'
import Link from 'next/link'

const Index = (props) => {
    console.log(props);
    return (
        <div>
            <strong>
                {props.user.name}
            </strong>
            <br />
            <strong>
                {props.user.cityusername}
            </strong>
            <br />
            <strong>
                {props.user.email}
            </strong>
            <br />
            <strong>
                {props.user.address.street}
            </strong>
            <br />
            <strong>
                {props.user.address.suite}
            </strong>
            <br />
            <strong>
                {props.user.address.city}
            </strong>
            <br />
            <strong>
                {props.user.address.zipcode}
            </strong>
            <br />
            <strong>
                {props.user.phone}
            </strong>
            <br />
            <strong>
                {props.user.website}
            </strong>
            <br />
            <strong>
                {props.user.company.name}
            </strong>

            <h1>Post</h1>
            <br />

            {props.post.map((e, i) => {
                return <div key={i}>
                    <Link href={`/post/${e.id}`}>
                        <a>{e.title}</a>
                    </Link>
                </div>


            }
            )}



        </div>
    )
}
export async function getServerSideProps(context) {
    const id = context.params.id
    const result = await request({
        method: "get",
        url: `https://jsonplaceholder.typicode.com/users?id=${id}`
    })
    if (result.status == 200) {
        const resultPosts = await request({
            method: "get",
            url: `https://jsonplaceholder.typicode.com/posts?userId=${id}`
        })
        return {
            props: {
                user: result.data[0],
                post: resultPosts.data
            }
        }
    }
    return {
        redirect: {
            permanent: false,
            destination: "/login",
        },
        props: {},
    };

}

export default Index