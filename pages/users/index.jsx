import request from '@api/request'
import Link from 'next/link'

const user = ({ user }) => {
    console.log("user", user);
    return (
        <div>
            {user.map((e, i) => {
                return (
                    <div key={i}>
                        <strong>
                            <Link href={`/users/${e.id}`}>
                                <a>{e.name}</a>
                            </Link>
                            <div>

                            </div>
                        </strong>
                        <strong>
                        {e.email}
                        </strong>
                        <br />
                        <br />
                    </div>
                )
            })}
        </div>
    )
}
export async function getServerSideProps(context) {
    const result = await request({
        method: "get",
        url: "https://jsonplaceholder.typicode.com/users"
    })
    if (result.status == 200) {
        return {
            props: {
                user: result.data
            }
        }
    }
    return {
        redirect: {
            permanent: false,
            destination: "/login",
        },
        props: {},
    };

}
export default user