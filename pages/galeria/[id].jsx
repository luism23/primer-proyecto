import request from '@api/request'

const Index = (props) => {
    console.log(props);
 return (
     <div> 
          {props.photos.map((e,i)=>{
                return (
                    <div key={i}>
                        <strong>
                            {e.title}
                        </strong>
                        <div>
                            <img src={e.url} alt="" />
                           
                        </div>
                        <br />
                        <br />
                    </div>
                )
            })}
    </div>
 )
}
export async function getServerSideProps(context) {
    const id = context.params.id
    const result = await request({
        method:"get",
        url:`https://jsonplaceholder.typicode.com/photos?albumId=${id}`
    })
    if(result.status == 200 ){
        return {
            props: {
                photos : result.data
          }
        }
    }
    return {
        // redirect: {
        //     permanent: false,
        //     destination: "/login",
        // },
        props: {},
    };

}

export default Index