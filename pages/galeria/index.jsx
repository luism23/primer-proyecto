import request from '@api/request'
import Link from 'next/link'
import Content from '@components/content'

const galeria = ({ albums }) => {
    return (
        <Content >
            <h1>info</h1>
            {albums.map((e, i) => {
                return (
                    <div key={i}>
                        <Link href={`/galeria/${e.id}`}>
                            <a >
                                <strong>
                                    {e.title}
                                </strong>

                            </a>
                        </Link>
                        <br />
                        <br />
                    </div>
                )
            })}
        </Content>
    )

}
export async function getServerSideProps(context) {
    const result = await request({
        method: "get",
        url: "https://jsonplaceholder.typicode.com/albums"
    })
    if (result.status == 200) {
        return {
            props: {
                albums: result.data
            }
        }
    }
    return {
        // redirect: {
        //     permanent: false,
        //     destination: "/login",
        // },
        props: {albums:[]},
    };

}

export default galeria