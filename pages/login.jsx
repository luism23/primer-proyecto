import { useState } from "react"

const Index = () => {
    const [contador, setContador] =  useState(1)

    const [data, setData] = useState({
        email:"1@1.com",
        password:""
    })
    const updateData = (id) => (e) => {
        var value = e
        if(value.target){
            value = value.target.value
        }
        setData({
            ...data,
            [id]:value
        })
    }
    const login = (e) => {
        e.preventDefault()
        console.log(data);
    }
    return (
        <div>
            <form onSubmit={login}>
                <label htmlFor="email" className="labelInput">
                    <span>Email</span>
                    <input
                        className="input"
                        type="email" name="email" id="email" 
                        value={data.email}
                        onChange={updateData("email")}
                        />
                </label>
                <label htmlFor="password" className="labelInput">
                    <span>Password</span>
                    <input
                        className="input"
                        type="password" name="password" id="password" 
                        value={data.password}
                        onChange={updateData("password")}
                        />
                </label>
                <br />
                <br />
                <input type="submit" value="Login"  className="btn"/>
            </form>
        </div>
    )
}
export default Index