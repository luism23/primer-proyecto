import request from '@api/request'

const Index = ({user}) => {
    console.log("user",user);
    return (
        <div>
            {user.map((e,i)=>{
                return (
                    <div key={i}>
                        <div>
                            {
                                e.id * 5
                            }
                        </div>
                        <strong>
                            {e.name}
                        </strong>
                        <div>
                            {e.email}
                        </div>
                        <br />
                        <br />
                    </div>
                )
            })}
        </div>
    )
}
export async function getServerSideProps(context) {
    const result = await request({
        method:"get",
        url:"https://jsonplaceholder.typicode.com/users"
    })
    if(result.status == 200 ){
        return {
            props: {
                user : result.data
          }
        }
    }
    return {
        redirect: {
            permanent: false,
            destination: "/login",
        },
        props: {},
    };

  }
export default Index