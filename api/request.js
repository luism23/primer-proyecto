import axios from 'axios'

const request = async (config) => {
    try {
        const result = await axios(config)
        return result
    } catch (error) {
        console.log(error);
        return error
    }
}
export default request