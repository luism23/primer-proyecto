import Header from '@components/header'
import Footer from '@components/footer'

const Index = (props) => {
    return (
        <>
            {
                props.header !== false 
                && 
                <Header/>
            }
            <div className="container">
                {props.children}
            </div>
            {
                props.footer !== false 
                && 
                <Footer/>
            }
        </>
    )
}
export default Index