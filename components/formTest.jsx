const Index = (props) => {
    const {
        action,
        numero,
    } = {
        action : () => {console.log("default");},
        numero:0,
        ...props
    }
    console.log(numero * numero);

    const detect = (e) => {
        const value = e.target.value
        if(value.indexOf("a") > -1){
            action(value)
        }
    }
    return (
        <>
            <input type="text" onChange={detect}/>
        </>
    )
}
export default Index