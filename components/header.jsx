import Link from "next/link"

const Index = () => {
    return (
        <>
            <header className="menu">
                <ul>
                    <li>
                        <Link href="/">
                            <a >
                                Home
                            </a>
                        </Link>
                    </li>
                    <li>
                        <Link href="/users">
                            <a >
                                User
                            </a>
                        </Link>
                    </li>
                    <li>
                        <Link href="/post">
                            <a >
                                Posts
                            </a>
                        </Link>
                    </li>
                    <li>
                        <Link href="/galeria">
                            <a >
                                Galeria
                            </a>
                        </Link>
                    </li>
                </ul>
            </header>
        </>
    )
}
export default Index